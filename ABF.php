<?php

namespace Org\Bundle\Carrier\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\RejectedPromise;
use GuzzleHttp\Psr7\Response;
use Org\Bundle\Annotation\CarrierClient;
use Org\Bundle\Annotation\CarrierClientField;
use Org\Bundle\Carrier\AbstractCarrierClient;
use Org\Bundle\Carrier\Exception\PostprocessingException;
use Org\Bundle\Carrier\Exception\PreprocessingException;
use Org\Bundle\Carrier\Exception\RuntimeException;
use Org\Bundle\Entity\Accessorial;
use Org\Bundle\Entity\RateRequest;
use Org\Bundle\Entity\RateResult;

/**
 * @CarrierClient(name="ABF (LTL)", slug="abf", scac="ABFS", contractType=1230)
 *
 * @see https://arcb.com/tools/api/rate-quote
 * @see https://www.abfs.com/xml/aquotexml.asp
 */
class ABF extends AbstractCarrierClient
{
    /*
     * List of specific limited access accessorial codes:
     * Acc_LAP 	"Y" for Limited Access Pickup option.
     * LAPType 	Type of Limited Access Pickup. Required for Acc_LAP:
     * Acc_LAD 	"Y" for Limited Access Delivery option.
     * LADType 	Type of Limited Access Delivery. Required for Acc_LAD:
     *   "C" for Church pickup.
     *   "M" for Military Site pickup.
     *   "S" for School pickup.
     *   "U" for Mini-Storage pickup.
     *   "O" for Other type of limited access pickup.
     */
    public static $accessorials = array(
        Accessorial::CONSTRUCTION_SITE_DELIVERY_ID => 'Acc_CSD',
        Accessorial::INSIDE_DELIVERY_ID => 'Acc_IDEL',
        Accessorial::INSIDE_PICKUP_ID => 'Acc_IPU',
        Accessorial::LIFTGATE_DELIVERY_ID => 'Acc_GRD_DEL',
        Accessorial::LIFTGATE_PICKUP_ID => 'Acc_GRD_PU',
        Accessorial::LIMITED_ACCESS_DELIVERY_ID => 'Acc_LAD',
        Accessorial::LIMITED_ACCESS_PICKUP_ID => 'Acc_LAP',
        Accessorial::NOTIFICATION_DELIVERY_ID => 'Acc_ARR',
        Accessorial::RESIDENTIAL_DELIVERY_ID => 'Acc_RDEL',
        Accessorial::RESIDENTIAL_PICKUP_ID => 'Acc_RPU',
        Accessorial::SINGLE_SHIPMENT_PICKUP_ID => 'Acc_SS',
    );

    protected $client;

    /**
     * @CarrierClientField(label="API ID")
     */
    protected $apiId;

    protected $billingCity;

    protected $billingState;

    protected $billingPostal;

    protected $termsPostalCodes;

    public const URL = 'https://www.abfs.com/xml/aquotexml.asp';

    public function __construct(Client $client, $apiId, $billingCity, $billingState, $billingPostal, $termsPostalCodes = null)
    {
        $this->client = $client;
        $this->apiId = $apiId;
        $this->billingCity = $billingCity;
        $this->billingState = $billingState;
        $this->billingPostal = $billingPostal;
        $this->termsPostalCodes = $termsPostalCodes;
    }

    public function getRequestData(RateRequest $rateRequest)
    {
        if (count($rateRequest->getItems()) > 15) {
            throw new PreprocessingException('Request has more than 15 items.');
        }
        $pickup = $rateRequest->getPickup();
        $delivery = $rateRequest->getDelivery();

        $data = array(
            'ID' => $this->apiId,
            'ShipCity' => $pickup->getCity(),
            'ShipState' => $pickup->getState(),
            'ShipZip' => $pickup->getPostal(),
            'ShipCountry' => $pickup->getCountry(),
            'ConsCity' => $delivery->getCity(),
            'ConsState' => $delivery->getState(),
            'ConsZip' => $delivery->getPostal(),
            'ConsCountry' => $delivery->getCountry(),
            'FrtLWHType' => 'IN',
            'Acc_PALLET' => 'Y', // palletized shipment - Doesn't show up as ACC in resonse, doesn't change price at all
        );

        $terms = 'third_party';

        if (is_array($this->termsPostalCodes) && count($this->termsPostalCodes)) {
            if (in_array($pickup->getPostal(), $this->termsPostalCodes)) {
                $terms = 'shipper';
            } elseif (in_array($delivery->getPostal(), $this->termsPostalCodes)) {
                $terms = 'consignee';
            }
        }

        switch ($terms) {
            case 'shipper':
                $data['ShipAff'] = 'Y';
                $data['ShipPay'] = 'Y';
                break;
            case 'consignee':
                $data['ConsAff'] = 'Y';
                $data['ConsPay'] = 'Y';
                break;
            case 'third_party':
            default:
                $data['TPBAff'] = 'Y';
                $data['TPBPay'] = 'Y';
                $data['TPBZip'] = $this->billingPostal;
                $data['TPBCity'] = $this->billingCity;
                $data['TPBState'] = $this->billingState;
                break;
        }

        if ($rateRequest->getIsHazmat()) {
            $data['Acc_HAZ'] = 'Y';
        }

        $i = 1;

        foreach ($rateRequest->getItems() as $item) {
            $data['Wgt'.$i] = $item->getWeight();
            $data['Class'.$i] = $item->getClass();
            $data['FrtLng'.$i] = $item->getLength(); // api expects inches
            $data['FrtWdth'.$i] = $item->getWidth(); // api expects inches
            $data['FrtHght'.$i] = $item->getHeight(); // api expects inches
            $data['UnitNo'.$i] = $item->getPallets();
            $data['UnitType'.$i] = 'PLT';

            ++$i;
        }

        if ($accessorials = $rateRequest->getAccessorials()) {
            foreach ($accessorials as $accessorial) {
                if (!array_key_exists($accessorial, self::$accessorials)) {
                    throw new PreprocessingException('Request has unsupported accessorial.');
                }

                if ($code = self::$accessorials[$accessorial]) {
                    $data[$code] = 'Y';

                    if ('Acc_LAD' === $code) {
                        $data['LADType'] = 'O';
                    } elseif ('Acc_LAP' === $code) {
                        $data['LAPType'] = 'O';
                    }
                }
            }
        }

        return $data;
    }

    public function getRateResultAsync(RateRequest $rateRequest)
    {
        try {
            $data = $this->getRequestData($rateRequest);
        } catch (\Exception $e) {
            return new RejectedPromise($e);
        }

        return $this->client
            ->postAsync(static::URL, array(
                'http_errors' => false,
                'form_params' => $data,
            ))
            ->then(array($this, 'process'))
        ;
    }

    public function process(Response $response)
    {
        if ($response->getStatusCode() >= 400) {
            throw new RuntimeException(sprintf('%s %s', $response->getStatusCode(), $response->getReasonPhrase()));
        }

        $data = simplexml_load_string($response->getBody());

        if (0 != (int) $data->NUMERRORS) {
            foreach ($data->ERROR as $error) {
                throw new RuntimeException(sprintf('Code: %s, Message: %s', $error->ERRORCODE, $error->ERRORMESSAGE));
            }
        }

        if (!$totalCost = (string) $data->CHARGE) {
            throw new PostprocessingException('No rate returned.');
        }

        if ('CONNECT' === (string) $data->ORIGTERMINFO->TYPE) {
            throw new PostprocessingException('Indirect pickup is unsupported.');
        }

        if ('CONNECT' === (string) $data->DESTTERMINFO->TYPE) {
            throw new PostprocessingException('Indirect delivery is unsupported.');
        }

        $rateResult = new RateResult();
        $rateResult
            ->setTotalCost($totalCost)
            ->setCarrierQuoteNumber((string) $data->QUOTEID)
        ;

        $transitTime = '';

        if (isset($data->ADVERTISEDTRANSIT)) {
            $transitTime = preg_replace('/(\D+)/', '', (string) $data->ADVERTISEDTRANSIT);
        } elseif (isset($data->ESTIMATEDTRANSIT)) {
            $transitTime = preg_replace('/(\D+)/', '', (string) $data->ESTIMATEDTRANSIT);
        } else {
            throw new RuntimeException(sprintf('No Transit Time found in response.'));
        }

        if (!is_numeric($transitTime)) {
            throw new RuntimeException('Transit time is not numeric.');
        }

        $rateResult->setTransitTime((int) $transitTime);

        if ('MIA' === (string) $data->ORIGTERMINFO->TYPE) {
            $rateResult->addLog('Origin service is MIA (served DIRECT via marketing partner)', 'None');
        }

        if ('MIA' === (string) $data->DESTTERMINFO->TYPE) {
            $rateResult->addLog('Destination service is MIA (served DIRECT via marketing partner)', 'None');
        }

        if (isset($data->SHIPMENTCUBE) && !empty($data->SHIPMENTCUBE)) {
            $aVolUnit = strtolower((string) $data->SHIPMENTCUBE->ACTUAL['UNIT']);
            $rVolUnit = strtolower((string) $data->SHIPMENTCUBE->RATED['UNIT']);
            $rateResult->addLog(
                sprintf(
                    'Actual: %s cu %s, Rated: %s cu %s',
                    $data->SHIPMENTCUBE->ACTUAL,
                    $aVolUnit,
                    $data->SHIPMENTCUBE->RATED,
                    $rVolUnit
                ),
                'None'
            );
        }

        if (isset($data->NOTES) && !empty($data->NOTES)) { // notes not set for ABF Volume Quotes
            foreach ($data->NOTES as $note) {
                if (!empty(trim($note))) {
                    $rateResult->addLog(sprintf('%s Note: %s', $note->NOTE['TYPE'], $note->NOTE), 'None');
                }
            }
        }

        if (isset($data->ITEMIZEDCHARGES)) {
            foreach ($data->ITEMIZEDCHARGES->ITEM as $accessorial) {
                $op = 'DISCOUNT' === (string) $accessorial['TYPE'] ? '-' : '+';
                $rateResult->addLog(
                    sprintf(
                        '%s: %s. %s',
                        $accessorial['FOR'],
                        $accessorial['DESCRIPTION'],
                        $accessorial['RATE']
                    ),
                    $op.' $'.$accessorial['AMOUNT']
                );
            }
        } elseif (isset($data->INCLUDEDCHARGES)) {
            foreach ($data->INCLUDEDCHARGES as $charge) {
                foreach ($charge as $name => $amount) {
                    if (!empty($amount)) {
                        $rateResult->addLog(sprintf('%s ', $name), '+ $'.$amount);
                    }
                }
            }
        }

        if (isset($data->QUOTEDESCRIPTION)) {
            $rateResult->addLog(sprintf('%s', $data->QUOTEDESCRIPTION), 'None.');
        }

        return $rateResult;
    }
}
