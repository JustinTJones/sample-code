<?php

namespace Org\Bundle\Carrier\Client;

use GuzzleHttp\Psr7\Response;
use Org\Bundle\Annotation\CarrierClient;
use Org\Bundle\Carrier\Exception\PreprocessingException;
use Org\Bundle\Entity\Accessorial;
use Org\Bundle\Entity\RateRequest;

/**
 * @CarrierClient(name="ABF (Volume)", slug="abf-volume", scac="ABFS", contractType=1240)
 *
 * @see https://arcb.com/tools/api/volume-quote
 * @see https://api.arcb.com/quotes/volume/xml/
 */
class ABFVolume extends ABF
{
    /* Remove "xml/" from the URL to get a JSON response. */
    public const URL = 'https://api.arcb.com/quotes/volume/xml/';

    public function getRequestData(RateRequest $rateRequest)
    {
        if (!$rateRequest->hasAllDimensions()) {
            throw new PreprocessingException('All dimensions are required.');
        }

        $data = parent::getRequestData($rateRequest);
        $i = 1;

        foreach ($rateRequest->getItems() as $item) {
            if (empty($data['Class'.$i])) {
                $data['Class'.$i] = '250';
            }

            ++$i;
        }

        /* Accessorials not supported in this API
            The API doesn't complain if they exist, so we could just ignore them.
            Accessorial::NOTIFICATION_DELIVERY_ID => 'Acc_ARR',
            Accessorial::SINGLE_SHIPMENT_PICKUP_ID => 'Acc_SS',
        */
        unset($data['Acc_ARR']);
        unset($data['Acc_SS']);

        $data['RequesterName'] = 'N/A';
        $data['RequesterEmail'] = 'noreply@example.com';

        if (array_key_exists('ConsPay', $data) && $data['ConsPay'] == 'Y') {
            $data['PayTerms'] = 'C';
        } else {
            $data['PayTerms'] = 'P';
        }

        return $data;
    }
}
