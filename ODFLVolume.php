<?php

namespace Org\Bundle\Carrier\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\RejectedPromise;
use GuzzleHttp\Psr7\Response;
use Org\Bundle\Annotation\CarrierClient;
use Org\Bundle\Annotation\CarrierClientField;
use Org\Bundle\Carrier\AbstractCarrierClient;
use Org\Bundle\Carrier\Exception\RuntimeException;
use Org\Bundle\Entity\Accessorial;
use Org\Bundle\Entity\RateRequest;
use Org\Bundle\Entity\RateResult;
use Org\Bundle\Library\SoapClient;

/**
 * @CarrierClient(name="Old Dominion (Volume)", slug="odfl-volume", scac="ODFL", contractType=1250)
 */
class ODFLVolume extends AbstractCarrierClient
{
    public static $accessorials = array(
        // Delivery service to mines: DSM
        // Construction site pickup: CSP
        // schools churches delivery: CDC
        // schools churches pickup: CPC
        // self storage delivery: SWD
        // trade show pickup: TRU
        // trade show delivery: TRO
        Accessorial::CONSTRUCTION_SITE_DELIVERY_ID => 'CSD',
        Accessorial::INSIDE_DELIVERY_ID => 'IDC',
        Accessorial::INSIDE_PICKUP_ID => 'IPC',
        Accessorial::LIFTGATE_DELIVERY_ID => 'HYD',
        Accessorial::LIFTGATE_PICKUP_ID => 'HYO',
        Accessorial::LIMITED_ACCESS_DELIVERY_ID => 'LDC', // Restricted Access Delivery
        Accessorial::LIMITED_ACCESS_PICKUP_ID => 'LPC', // Restricted Access Pickup
        Accessorial::NOTIFICATION_DELIVERY_ID => 'ARN',
        Accessorial::RESIDENTIAL_DELIVERY_ID => 'RDC',
        Accessorial::RESIDENTIAL_PICKUP_ID => 'RPC',
        Accessorial::SINGLE_SHIPMENT_PICKUP_ID => '', // No code, but allow it through.
    );

    protected $soapClient;

    protected $client;

    /**
     * @CarrierClientField
     */
    protected $username;

    /**
     * @CarrierClientField(private=true)
     */
    protected $password;

    /**
     * @CarrierClientField
     */
    protected $accountNumber;

    public function __construct(Client $client, $username, $password, $accountNumber)
    {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
        $this->accountNumber = $accountNumber;
    }

    public function getRateResultAsync(RateRequest $rateRequest)
    {
        // entire shipment cannot exceed 50000 lbs. (thrown by API)
        // must be less than 24000 lbs (inferred)
        if ($rateRequest->getTotalWeight() >= 24000) {
            return new RejectedPromise('Total weight cannot exceed 24,000 lbs.');
        }

        if (!$rateRequest->hasAllDimensions()) {
            return new RejectedPromise('All dimensions are required.');
        }

        // Linear Feet must be within range 3-56 (thrown by API)
        // must be less than 348 inches (30 ft) (inferred)
        $totalLength = $rateRequest->getLinearInches();

        if ($totalLength < (12 * 3)) {
            return new RejectedPromise('Total length must be more than 3 ft.');
        } elseif ($totalLength >= (12 * 30)) {
            return new RejectedPromise('Total length must be less than 30 ft.');
        }

        $pickup = $rateRequest->getPickup();
        $delivery = $rateRequest->getDelivery();
        $accessorialCodes = array();
        $maxLength = 0;

        foreach ($rateRequest->getItems() as $item) {
            $maxLength = max($maxLength, $item->getLength(), $item->getWidth());
        }

        if ($maxLength >= (12 * 20)) {
            // oversized: over 20 feet
            $accessorialCodes[] = 'OV5';
        } elseif ($maxLength >= (12 * 12)) {
            // oversized: between 12 and 20 feet
            $accessorialCodes[] = 'OV3';
        } elseif ($maxLength >= (12 * 8)) {
            // oversized: between 8 and 12 feet
            $accessorialCodes[] = 'OV1';
        }

        if ($rateRequest->getIsHazmat()) {
            $accessorialCodes[] = 'HAZ';
        }

        if ($accessorials = $rateRequest->getAccessorials()) {
            foreach ($accessorials as $accessorial) {
                if (array_key_exists($accessorial, self::$accessorials)) {
                    if (!empty(self::$accessorials[$accessorial])) {
                        $accessorialCodes[] = self::$accessorials[$accessorial];
                    }
                } else {
                    return new RejectedPromise('Request has unsupported accessorial.');
                }
            }
        }

        $requestData = array(
            'account' => $this->accountNumber,
            'reqCode' => 'R', // R=rate, E=email, Q=quote
            'rateId' => 0,
            'consigneeNumber' => '5555555555',
            'shipperNumber' => '5555555555',
            'originCity' => $pickup->getCity(),
            'originState' => $pickup->getState(),
            'originZip' => $pickup->getPostal(),
            'originCountry' => $pickup->getAlpha3CountryCode(),
            'destinationCity' => $delivery->getCity(),
            'destinationState' => $delivery->getState(),
            'destinationZip' => $delivery->getPostal(),
            'destinCountry' => $delivery->getAlpha3CountryCode(),
            'linearFeet' => (int) ceil($totalLength / 12),
            'weight' => $rateRequest->getTotalWeight(),
            'accsel' => implode('', $accessorialCodes),
        );

        try {
            $this->soapClient = new SoapClient('https://www.odfl.com/spotquote-service/spotquoteWS?wsdl', array('login' => $this->username, 'password' => $this->password));
        } catch (\SoapFault $e) {
            return new RejectedPromise($e);
        }

        $rateResponseData = array();
        $rateRequest = $this->soapClient->getRequest('getExtendedSpotquote', array('arg0' => $requestData));

        return $this->client
            ->sendAsync($rateRequest, array('http_errors' => false)) // send rate request
            ->then(function (Response $rateResponse) use (&$requestData, &$rateResponseData) {
                // handle rate response and send quote request
                $rateResponseData = $this->soapClient->getResponse('getExtendedSpotquote', $rateResponse)->return;

                if (property_exists($rateResponseData, 'errorMessage') && $errorMessage = $rateResponseData->errorMessage) {
                    throw new RuntimeException($errorMessage);
                }

                $requestData['rateId'] = $rateResponseData->rateId;
                $requestData['reqCode'] = 'Q'; // R=rate, E=email, Q=quote

                $quoteRequest = $this->soapClient->getRequest('getExtendedSpotquote', array('arg0' => $requestData));

                return $this->client->sendAsync($quoteRequest, array('http_errors' => false));
            })
            ->then(function ($quoteResponse) use (&$rateResponseData) {
                // handle quote response and build rateResult with both responses
                $quoteResponseData = $this->soapClient->getResponse('getExtendedSpotquote', $quoteResponse)->return;

                if (property_exists($quoteResponseData, 'errorMessage') && $errorMessage = $quoteResponseData->errorMessage) {
                    throw new RuntimeException($errorMessage);
                }

                return $this->process($rateResponseData, $quoteResponseData);
            })
        ;
    }

    public function process($rate, $quote)
    {
        $rateResult = new RateResult();
        $rateResult
            ->setTotalCost(str_replace(',', '', $rate->newTotal))
            ->setCarrierQuoteNumber($quote->quoteNumber)
            ->setTransitTime($rate->serviceDays)
        ;

        // Accessorials sent as {acc_code}{rate_calc}{description}{whitespace}{cost}
        // /(([A-Z]{3})([A-Z\s]{4})([a-zA-Z:\/\-\s]+)(\d{7}))/
        preg_match_all('/((?<code>[A-Z]{3})(?<rate>[A-Z\s]{4})(?<desc>[a-zA-Z:\'"\/\-\s]+)(?<cost>\d{7}))/', $rate->accdtl, $matches, PREG_SET_ORDER);

        foreach ($matches as $acc) {
            $cost = ltrim($acc['cost'], '0') / 100;
            $rateResult->addLog(sprintf('%s: %s (rate: %s)', $acc['code'], rtrim($acc['desc']), rtrim($acc['rate'])), '+ $'.$cost);
        }

        $rateResult->addLog(sprintf('Accessorials Subtotal: $%s', $rate->accessorialTotal), 'None.');
        $rateResult->addLog(sprintf('Rate ID: %s', $rate->rateId), 'None.');

        return $rateResult;
    }
}
