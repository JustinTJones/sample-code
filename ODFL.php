<?php

namespace Org\Bundle\Carrier\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\RejectedPromise;
use GuzzleHttp\Psr7\Response;
use Org\Bundle\Annotation\CarrierClient;
use Org\Bundle\Annotation\CarrierClientField;
use Org\Bundle\Carrier\AbstractCarrierClient;
use Org\Bundle\Carrier\Exception\RuntimeException;
use Org\Bundle\Entity\Accessorial;
use Org\Bundle\Entity\RateRequest;
use Org\Bundle\Entity\RateResult;
use Org\Bundle\Library\SoapClient;

/**
 * @CarrierClient(name="Old Dominion", slug="odfl", scac="ODFL", contractType=1260)
 */
class ODFL extends AbstractCarrierClient
{
    public static $accessorials = array(
        // Delivery service to mines: DSM
        // Construction site pickup: CSP
        // schools churches delivery: CDC
        // schools churches pickup: CPC
        // self storage delivery: SWD
        // trade show pickup: TRU
        // trade show delivery: TRO
        Accessorial::CONSTRUCTION_SITE_DELIVERY_ID => 'CSD',
        Accessorial::INSIDE_DELIVERY_ID => 'IDC',
        Accessorial::INSIDE_PICKUP_ID => 'IPC',
        Accessorial::LIFTGATE_DELIVERY_ID => 'HYD',
        Accessorial::LIFTGATE_PICKUP_ID => 'HYO',
        Accessorial::LIMITED_ACCESS_DELIVERY_ID => 'LDC', // Restricted Access Delivery
        Accessorial::LIMITED_ACCESS_PICKUP_ID => 'LPC', // Restricted Access Pickup
        Accessorial::NOTIFICATION_DELIVERY_ID => 'ARN',
        Accessorial::RESIDENTIAL_DELIVERY_ID => 'RDC',
        Accessorial::RESIDENTIAL_PICKUP_ID => 'RPC',
        Accessorial::SINGLE_SHIPMENT_PICKUP_ID => '', // No code, but allow it through.
    );

    protected $soapClient;

    protected $client;

    /**
     * @CarrierClientField
     */
    protected $username;

    /**
     * @CarrierClientField(private=true)
     */
    protected $password;

    /**
     * @CarrierClientField
     */
    protected $accountNumber;

    /**
     * @CarrierClientField(required=false, help="Leave blank for LTL (559). Use 4848 for pallet pricing.")
     */
    protected $tariff;

    public function __construct(Client $client, $username, $password, $accountNumber, $tariff = null)
    {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
        $this->accountNumber = $accountNumber;
        $this->tariff = $tariff;
    }

    public function getRateResultAsync(RateRequest $rateRequest)
    {
        // entire shipment cannot exceed 50000 lbs. (thrown by API)
        if ($rateRequest->getTotalWeight() > 50000) {
            return new RejectedPromise('Total weight cannot exceed 50,000 lbs.');
        }

        $totalLength = $rateRequest->getLinearInches();

        if ($totalLength > (12 * 52)) {
            return new RejectedPromise('Total length must not be more than 52 ft.');
        }

        $pickup = $rateRequest->getPickup();
        $delivery = $rateRequest->getDelivery();
        $accessorialCodes = array();
        $freightItems = array();
        $maxLength = 0;

        foreach ($rateRequest->getItems() as $item) {
            $maxLength = max($maxLength, $item->getLength(), $item->getWidth());
            $freightItem = array(
                'ratedClass' => $item->getClass(), // required
                'weight' => $item->getWeight(), // required
                'dimensionUnits' => 'IN', // 'IN' or 'FT'
                'numberOfUnits' => $item->getPallets(),
            );

            if ($item->getLength()) {
                $freightItem['length'] = $item->getLength();
            }

            if ($item->getWidth()) {
                $freightItem['width'] = $item->getWidth();
            }

            if ($item->getHeight()) {
                $freightItem['height'] = $item->getHeight();
            }

            // if shipment is going to mexico actualClass is required

            $freightItems[] = $freightItem;
        }

        if ($maxLength >= (12 * 20)) {
            // oversized: over 20 feet
            $accessorialCodes[] = 'OV5';
        } elseif ($maxLength >= (12 * 12)) {
            // oversized: between 12 and 20 feet
            $accessorialCodes[] = 'OV3';
        } elseif ($maxLength >= (12 * 8)) {
            // oversized: between 8 and 12 feet
            $accessorialCodes[] = 'OV1';
        }

        if ($rateRequest->getIsHazmat()) {
            $accessorialCodes[] = 'HAZ';
        }

        if ($accessorials = $rateRequest->getAccessorials()) {
            foreach ($accessorials as $accessorial) {
                if (array_key_exists($accessorial, self::$accessorials)) {
                    if (!empty(self::$accessorials[$accessorial])) {
                        $accessorialCodes[] = self::$accessorials[$accessorial];
                    }
                } else {
                    return new RejectedPromise('Request has unsupported accessorial.');
                }
            }
        }

        $requestData = array(
            'odfl4MeUser' => $this->username,
            'odfl4MePassword' => $this->password,
            'odflCustomerAccount' => $this->accountNumber,
            'requestReferenceNumber' => true, // Do you want a quote/reference number generated and returned? We request you only generate quotes when you are likely to use them.
            'originCity' => $pickup->getCity(),
            'originState' => $pickup->getState(),
            'originPostalCode' => $pickup->getPostal(),
            'originCountry' => $pickup->getAlpha3CountryCode(),
            'destinationCity' => $delivery->getCity(),
            'destinationState' => $delivery->getState(),
            'destinationPostalCode' => $delivery->getPostal(),
            'destinationCountry' => $delivery->getAlpha3CountryCode(),
            'accessorials' => $accessorialCodes,
            'tariff' => $this->tariff, // defaults to 559
            'numberPallets' => $rateRequest->getTotalPallets(), // testing for pallet (4848) pricing
            'freightItems' => $freightItems,
        );

        try {
            $this->soapClient = new SoapClient('https://www.odfl.com/wsRate_v6/RateService?wsdl');
        } catch (\SoapFault $e) {
            return new RejectedPromise($e);
        }

        $request = $this->soapClient->getRequest('getLTLRateEstimate', array('arg0' => $requestData));

        return $this->client
            ->sendAsync($request, array('http_errors' => false)) // send rate request
            ->then(array($this, 'process'))
        ;
    }

    public function process(Response $response)
    {
        $data = $this->soapClient->getResponse('getLTLRateEstimate', $response)->return;

        if (property_exists($data, 'errorMessages') && $errorMessage = $data->errorMessages) {
            throw new RuntimeException($errorMessage);
        }

        $rateResult = new RateResult();
        $rateResult
            ->setTotalCost($data->rateEstimate->netFreightCharge)
            ->setCarrierQuoteNumber($data->referenceNumber)
            ->setTransitTime($data->destinationCities->serviceDays)
        ;

        $rateResult->addLog('Gross FreightCharge: ', '+ $'.$data->rateEstimate->grossFreightCharge);
        $rateResult->addLog(sprintf('Discount (%s%%)', $data->rateEstimate->discountPercentage), '- $'.$data->rateEstimate->discountAmount);
        $rateResult->addLog('Fuel Surcharge', '+ $'.$data->rateEstimate->fuelSurcharge);

        if (property_exists($data->rateEstimate, 'accessorialCharges')) {
            $accessorials = $data->rateEstimate->accessorialCharges;

            if (!is_array($accessorials)) {
                $accessorials = array($accessorials);
            }

            foreach ($accessorials as $accessorial) {
                $rateResult->addLog(sprintf('%s', $accessorial->description), '+ $'.$accessorial->amount);
            }
        }

        $rateResult->addLog(sprintf('Accessorials Total: $%s'.(($data->rateEstimate->variableAccessorialRate) ? ' (Variable Rate)' : ''), $data->rateEstimate->totalAccessorialCharge), 'None');

        return $rateResult;
    }
}
