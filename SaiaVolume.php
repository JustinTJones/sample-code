<?php

namespace Org\Bundle\Carrier\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\RejectedPromise;
use GuzzleHttp\Psr7\Response;
use Org\Bundle\Annotation\CarrierClient;
use Org\Bundle\Annotation\CarrierClientField;
use Org\Bundle\Carrier\AbstractCarrierClient;
use Org\Bundle\Carrier\Exception\PostprocessingException;
use Org\Bundle\Carrier\Exception\RuntimeException;
use Org\Bundle\Entity\Accessorial;
use Org\Bundle\Entity\RateRequest;
use Org\Bundle\Entity\RateResult;

/**
 * @CarrierClient(name="Saia (Volume)", slug="saia-volume", scac="SAIA", contractType=1220)
 *
 * @see https://www.saiasecure.com/webservice/spotquote/v1/saiaratequote.htm
 * @see https://apidev.saia.com/spotquote/v1/saiaratequote.htm
 */
class SaiaVolume extends AbstractCarrierClient
{
    public static $accessorials = array(
        Accessorial::CONSTRUCTION_SITE_DELIVERY_ID => 'LimitedAccessLocation', // Limited Access Delivery
        Accessorial::INSIDE_DELIVERY_ID => 'InsideDelivery',
        Accessorial::INSIDE_PICKUP_ID => 'InsidePickup',
        Accessorial::LIFTGATE_DELIVERY_ID => 'LiftgateService',
        Accessorial::LIFTGATE_PICKUP_ID => 'LiftgateServicePU',
        Accessorial::LIMITED_ACCESS_DELIVERY_ID => 'LimitedAccessLocation',
        Accessorial::LIMITED_ACCESS_PICKUP_ID => 'LimitedAccessLocationPU',
        Accessorial::NOTIFICATION_DELIVERY_ID => 'ArrivalNotice/Appointment',
        Accessorial::RESIDENTIAL_DELIVERY_ID => 'ResidentialDelivery',
        Accessorial::RESIDENTIAL_PICKUP_ID => 'ResidentialPickup',
        Accessorial::SINGLE_SHIPMENT_PICKUP_ID => 'SingleShipment',
    );

    protected $client;

    /**
     * @CarrierClientField
     */
    protected $username;

    /**
     * @CarrierClientField(private=true)
     */
    protected $password;

    /**
     * @CarrierClientField(label="TMS ID", required=false)
     */
    protected $tmsid;

    protected $test;

    public function __construct(Client $client, $username, $password, $tmsid = null, $test = false)
    {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
        $this->tmsid = $tmsid;
        $this->test = $test;
    }

    public function getRateResultAsync(RateRequest $rateRequest)
    {
        if ($this->test) {
            // Testing URL
            $url = 'https://apidev.saia.com/spotquote/v1/api/SpotQuoteRequest';
        } else {
            // Prod URL
            $url = 'https://www.saiasecure.com/webservice/spotquote/v1/api/SpotQuoteRequest';
        }

        if (!$rateRequest->hasAllDimensions()) {
            return new RejectedPromise('All dimensions are required.');
        }

        /*
         * Error Code: S03
         * Message: Spot Quote Engine Error: Successfully queried API, but could not generate spot quote.
         * Error Reason: Spot quotes can only be quoted for shipments up to 28 feet and between 500 and 24,000 lbs.
         */
        if (($rateRequest->getLinearInches() / 12) > 28) {
            return new RejectedPromise('Request must be less than 28 feet long.');
        }
        if ($rateRequest->getTotalWeight() < 500 || $rateRequest->getTotalWeight() > 24000) {
            return new RejectedPromise('Request must be between 500 and 24,000 lbs.');
        }

        $pickup = $rateRequest->getPickup();
        $delivery = $rateRequest->getDelivery();

        $data = array(
            'Accessorials' => array(),
            'UserID' => $this->username,
            'Password' => $this->password,
            'OriginCity' => $pickup->getCity(),
            'OriginState' => $pickup->getState(),
            'OriginZip' => $pickup->getPostal(),
            'DestCity' => $delivery->getCity(),
            'DestState' => $delivery->getState(),
            'DestZip' => $delivery->getPostal(),
            'LinearFeet' => $rateRequest->getLinearInches() / 12,
            'TotalWeight' => $rateRequest->getTotalWeight(),
            'HandlingUnits' => $rateRequest->getTotalPallets(),
            'TMSID' => $this->tmsid,
        );

        if ($rateRequest->getIsHazmat()) {
            $data['Accessorials'][] = array('Code' => 'Hazardous');
        }

        // Get maximum class for shipment or 250 if not set
        $class = null;

        foreach ($rateRequest->getItems() as $item) {
            $class = max($class, $item->getClass());
        }

        if (empty($class)) {
            $class = '250';
        }

        $data['FreightClass'] = $class;

        if ($accessorials = $rateRequest->getAccessorials()) {
            foreach ($accessorials as $accessorial) {
                if (!array_key_exists($accessorial, self::$accessorials)) {
                    return new RejectedPromise('Request has unsupported accessorial.');
                }

                if ($code = self::$accessorials[$accessorial]) {
                    $data['Accessorials'][] = array('Code' => $code);
                }
            }
        }

        return $this->client
            ->postAsync($url, array(
                'http_errors' => false,
                'json' => $data,
            ))
            ->then(array($this, 'process'))
        ;
    }

    public function process(Response $response)
    {
        if ($response->getStatusCode() >= 400) {
            throw new RuntimeException(sprintf('%s %s', $response->getStatusCode(), $response->getReasonPhrase()));
        }

        $data = json_decode($response->getBody(), true);

        if (!empty($data['ErrorCode'])) {
            throw new RuntimeException(sprintf('Code: %s, Message: %s', $data['ErrorCode'], $data['ErrorMessage']));
        }

        if (!$totalCost = (string) $data['TotalNetInvoice']) {
            throw new PostprocessingException('No rate returned.');
        }

        $transitTime = (string) $data['StandardServiceDays'];

        if (!is_numeric($transitTime)) {
            throw new RuntimeException('Transit time is not numeric.');
        }

        $rateResult = new RateResult();
        $rateResult
            ->setTotalCost($totalCost)
            ->setTransitTime((int) $transitTime)
            ->setCarrierQuoteNumber((string) $data['QuoteNumber'])
        ;

        if (true === $this->test) {
            $rateResult->addLog('WARNING: This rate was generated using test mode and should not be dispatched.', 'None');
        }

        $rateResult->addLog('Base Cost (before accessorials and fuel).', '+ $'.$data['ChargePrice']);

        foreach ($data['RateAccessorials'] as $accessorial) {
            $rateResult->addLog(sprintf('%s: %s', $accessorial['Code'], $accessorial['Description']), '+ $'.$accessorial['Amount']);
        }

        $rateResult->addLog(sprintf('Fuel Surcharge (%s%%)', $data['FuelSurchargeRate']), '+ $'.$data['FuelSurcharge']);

        return $rateResult;
    }
}
